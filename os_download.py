from mundilib import mundiCatalogue
from mundilib import mundiopenURL2
from mundilib import findentries
from mundilib import mundi_nsmap
from urllib.parse import urlparse
import subprocess


def gen_polygon(coord):
    """
    generate polygon string for the os query
    Arguments:
        coord list(2-uple(float, float)): list of coordinates
    """
    coord.append(coord[0])
    return ", ".join([f"{i[0]} {i[1]}" for i in coord])


def gen_s3_url(http_url):
    o = urlparse(http_url)
    print(o)
    return f"s3:/{o.path}"


def get_folder_or_zip(polygon):
    # q = "q=(onlineStatus:(ONLINE OR STAGING) AND footprint:\"intersects(POLYGON((-2.15 49.84, -0.35 49.84, -0.35 51.11, -2.15 51.11, -2.15 49.84)))\" AND sensingStartDate:[2019-04-07T00:00:00Z TO 2019-04-07T23:59:59Z])&processingLevel=L1C"
    q = f"q=(onlineStatus:(ONLINE OR STAGING) AND footprint:\"intersects(POLYGON(({polygon})))\" AND sensingStartDate:[2019-04-07T00:00:00Z TO 2019-04-07T23:59:59Z])&processingLevel=L1C"

    c = mundiCatalogue()
    col = c.getCollection("Sentinel2")
    response_wrapper_list = mundiopenURL2(col=col, query=q)
    entries = findentries(response_wrapper_list)
    print (len(entries))
    # obj = entries[:1][0]
    # print(entries)
    for entry in entries:
        # obj = entry[0]
        link = entry.findall("atom:link[@rel='enclosure']", namespaces=mundi_nsmap)
        url = link[0].attrib['href']
        print(url)
        s3_uri = gen_s3_url(url)
        print(s3_uri)
        cmd = ['s3cmd', 'get', '-r', '--force', s3_uri]
        subprocess.run(cmd)  # doesn't capture output


if __name__ == '__main__':
    coord = [(-2.15, 49.84), (-0.35, 49.84), (-0.35, 51.11), (-2.15, 51.11)]
    polygon = gen_polygon(coord)
    get_folder_or_zip(polygon)
